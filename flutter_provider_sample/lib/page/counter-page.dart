import 'package:flutter/material.dart';
import 'package:flutter_provider_sample/bloc/counter-bloc.dart';
import 'package:flutter_provider_sample/widget/decrement.dart';
import 'package:flutter_provider_sample/widget/increment.dart';
import 'package:provider/provider.dart';

class CounterPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final CounterBloc counterBloc = Provider.of<CounterBloc>(context);
    return Scaffold(
        body: new Container(
            child: Center(
                child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          counterBloc.counter.toString(),
          style: TextStyle(fontSize: 62.0),
        ),
        IncrementButton(),
        DecrementButton()
      ],
    ))));
  }
}
