import 'package:flutter/material.dart';
import 'package:flutter_provider_sample/bloc/counter-bloc.dart';
import 'package:provider/provider.dart';

class DecrementButton extends StatelessWidget {

  Widget build(BuildContext context) {
    final CounterBloc counterBloc = Provider.of<CounterBloc>(context);

    return new FlatButton.icon(
      icon: Icon(Icons.remove),
      label: Text('Remove'),
      onPressed: () => counterBloc.decrement(),
    );
  }
}