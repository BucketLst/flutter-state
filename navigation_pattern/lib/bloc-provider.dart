import 'package:flutter/material.dart';

import 'generic-bloc.dart';

class BlocProvider extends InheritedWidget {
  final BlocBase bloc;
  final Widget child;
  BlocProvider({this.bloc, this.child}) : super(child: child);

  static BlocProvider of(BuildContext context) =>
      (context.inheritFromWidgetOfExactType(BlocProvider) as BlocProvider);
  @override
  bool updateShouldNotify(BlocProvider oldWidget) {
    return bloc != oldWidget.bloc;
  }
}