import 'package:flutter/material.dart';
import 'package:navigation_pattern/generic-bloc.dart';

import 'counter_bloc.dart';

class SecondRoute extends StatefulWidget {
    @override
  _SecondRouteState createState() => _SecondRouteState();
}

class _SecondRouteState extends State<SecondRoute> {

  CounterBloc bloc;

    @override
  void dispose(){
    super.dispose();
    // bloc.dispose();
  }

  @override
  Widget build(BuildContext context) {
    CounterBloc bloc = BlocProvider.of<CounterBloc>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Second Route'),
      ),
      body: Center(
        child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
        StreamBuilder(
              initialData: bloc.data,
              stream: bloc.streamcounter,
              builder: (BuildContext context, AsyncSnapshot<int> snapshot) =>
                  Text(
                    snapshot?.data?.toString(),
                    style: Theme.of(context).textTheme.headline4,
                  )),
        RaisedButton(
          child: Text('Back'),
          onPressed: () {
            Navigator.pop(context);
          },
        )],
      )
      ),
    );
  }
}
