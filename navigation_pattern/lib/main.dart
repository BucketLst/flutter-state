import 'package:flutter/material.dart';
import 'package:navigation_pattern/counter_bloc.dart';
import 'package:navigation_pattern/first-route.dart';
import 'package:navigation_pattern/seond-route.dart';



 import 'generic-bloc.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      bloc: CounterBloc(),
      child: MaterialApp(
            title: 'Flutter Demo',
            routes: {
              '/first': (context) => FirstRoute(),
              '/sec': (context) => SecondRoute()
            },
            initialRoute: '/first',
            theme: ThemeData(
              primarySwatch: Colors.blue,
              // This makes the visual density adapt to the platform that you run
              // the app on. For desktop platforms, the controls will be smaller and
              // closer together (more dense) than on mobile platforms.
              visualDensity: VisualDensity.adaptivePlatformDensity,
            ),
    ));
  }
}

