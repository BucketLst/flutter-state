import 'package:flutter/material.dart';
import 'package:navigation_pattern/counter_bloc.dart';
import 'package:navigation_pattern/generic-bloc.dart';
import 'package:navigation_pattern/seond-route.dart';
// import 'package:navigation_pattern/counter_bloc.dart';
// import 'package:navigation_pattern/generic-bloc.dart';

class FirstRoute extends StatefulWidget {
    @override
  _FirstRouteState createState() => _FirstRouteState();
}

class _FirstRouteState extends State<FirstRoute> {
CounterBloc bloc;

  @override
  void dispose(){
    super.dispose();
    // bloc.dispose();
  }

  @override
  Widget build(BuildContext context) {
    bloc = BlocProvider.of<CounterBloc>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('First Route'),
      ),
      body: Center(
        child: RaisedButton(
          child: Text('update & move for result'),
          onPressed: () {
            bloc.updateCount();
             Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => SecondRoute()),
            );
          },
        ),
      ),
    );
  }
}
