

import 'dart:async';

import 'package:rxdart/subjects.dart';

import 'generic-bloc.dart';


class CounterBloc implements  BlocBase {

  int data=0;

  StreamController<int> _counterStreamController =  BehaviorSubject();

  StreamSink<int> get countersink => _counterStreamController.sink;

  Stream<int> get streamcounter => _counterStreamController.stream;

  void updateCount() {
    data=data+1;
    countersink.add(data);
  }

  @override
  void dispose() {
    _counterStreamController?.close();
  }

}