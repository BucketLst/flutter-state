

import 'dart:async';

import 'package:count_pattern/base_bloc.dart';

class CounterBloc implements BaseBloc {

  int data=0;

  StreamController<int> _counterStreamController =  StreamController();

  StreamSink<int> get countersink => _counterStreamController.sink;

  Stream<int> get streamcounter => _counterStreamController.stream;

  void updateCOunt() {
    data=data+1;
    countersink.add(data);
  }

  @override
  void dispose() {
    _counterStreamController.close();
  }

}